import api from '../modules/api'

export const USER_LOGIN_BEGIN   = 'USER_LOGIN_BEGIN';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAILURE = 'USER_LOGIN_FAILURE';

export const userLoginBegin = () => ({
  type: USER_LOGIN_BEGIN
});

export const userLoginSuccess = student => ({
  type: USER_LOGIN_SUCCESS,
  payload: { student }
});

export const userLoginFailure = error => ({
  type: USER_LOGIN_FAILURE,
  payload: { error }
});

export function userLogin(data) {
    console.log('post ',JSON.stringify(data))
    return dispatch => {
      dispatch(userLoginBegin());
      return fetch("http://localhost:8888/api/user/login", {
        headers: {
          'Content-type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(data)
      })
        .then(handleErrors)
        .then(res => res.json())
        .then(json => {
          dispatch(userLoginSuccess(json.student));
          return json.student;
        })
        .catch(error => dispatch(userLoginFailure(error)));
    };
  }
  
  // Handle HTTP errors since fetch won't.
  function handleErrors(response) {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  }