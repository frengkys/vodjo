import React, { Component } from 'react'
import LoginForm from '../component/Login'
import { Grid, Jumbotron, Button} from 'react-bootstrap'

class Home extends Component {
  render() {
    return (
        <Grid style={{ textAlign: 'center'}}>
            <LoginForm />
        </Grid>
    )
  }
}

export default Home