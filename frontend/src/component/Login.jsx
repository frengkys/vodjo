import React, { Component } from 'react';
import { connect } from "react-redux";
import { userLogin } from "../action/userLoginAction";
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {
                username: '',
                password: '',
            },
            loading: false,
            errors: {}
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
    this.setState({
        data: {
        ...this.state.data, [e.target.name]: e.target.value
        }
    });
    }

    handleSubmit(e) {
        e.preventDefault();
        this.props.userLogin(this.state.data).then( () => window.location.reload() );
    }

    render() {
        return (
            <form action="" onSubmit={ this.handleSubmit }>
                <div>
                    <label htmlFor="email">Email: </label>
                    <input type="email" name="email" value={ this.state.username } onChange={this.handleChange} />
                </div>
                <div>
                    <label htmlFor="password">Password: </label>
                    <input type="password" name="password" value={ this.state.password } onChange={this.handleChange} />
                </div>
            </form>
        );
    }
}

const mapStateToProps = state => ({
    // students: state.students.items,
    // loading: state.students.loading,
    // error: state.students.error,
});

export default connect(mapStateToProps, { userLogin })(Login);