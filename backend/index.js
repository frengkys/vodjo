import express from 'express'
import logger from 'morgan'
import router from './src/routes/index'
import compression from 'compression'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import db from './src/models'
import passport from 'passport'
import formidable from 'express-formidable'
import {} from 'dotenv/config'
import strategy from './src/config/passport'

let app = express()
const port = process.env.PORT || 8888

let server = require('http').createServer(app);
let io = require('socket.io')(server);

app.use(formidable())
app.use(logger('dev'))
app.use(cookieParser())
app.use(compression())
app.use(cors())
passport.use(strategy);
app.use(passport.initialize())

app.use('/api', router)

app.listen(port, function () {
    // dev mode
    db.sequelize.sync({force:false})
    console.log(`Server listening on port  ${port}!`)
})

export default app