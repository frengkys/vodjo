'use strict';
module.exports = (sequelize, DataTypes) => {
  var Journal = sequelize.define('Journal', {
    day: DataTypes.DATE,
    task: DataTypes.STRING,
    task_time: DataTypes.TIME
  }, {});
  Journal.associate = function(models) {
    // associations can be defined here
  };
  return Journal;
};