import passportJWT from 'passport-jwt'
import { User } from '../models'

const ExtractJwt = passportJWT.ExtractJwt
const JwtStrategy = passportJWT.Strategy

var jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
jwtOptions.secretOrKey = process.env.SECRET

module.exports =  new JwtStrategy(jwtOptions,async function(jwtPayload, done) {
    let temp =  await User.findOne({ where: {email: jwtPayload.email} })
    return ( !temp ) ? done(temp) : done(null, temp)
})