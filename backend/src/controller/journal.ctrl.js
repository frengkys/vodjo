import { Journal } from '../models/'
import { getToken } from '../library/util'

function getAll(req, res) {
    Journal.findAll().then(journal => {
        return res.json(journal);
    })
}

function getByDate(req, res) {

}

function store(req, res) {
    const data = {
        task : req.fields.task,
        day : req.fields.day,
    }

    Journal.create( data )
    .then( journal => {
        return res.json({message: 'success', journal});
    })
    .catch( error => {
        console.log('Err ', error);
    });

}

export default {
    getAll,
    store,
    getByDate
}