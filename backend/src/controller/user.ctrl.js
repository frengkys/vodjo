import { User } from '../models/'
import bcrypt from 'bcryptjs'
import { getToken } from '../library/util'

/**
 * Get user
 * @returns {User}
 */

function getAll(req, res) {
    User.findAll().then(users => {
        return res.json(users);
    })
}

function register(req, res) {
    const data = {
        email : req.fields.email,
        status : req.fields.status, // 1 = directur || 0 karyawan
        password : req.fields.password,
    }
    data['password']= bcrypt.hashSync( data.password , 8)
    User.create( data )
    .then( user => {
        res.json({
            message: 'New user registered ', 
            user, 
            token: getToken({email: user.email})})
        })
    .catch( error => {
        console.log('Err ', error);
        return res.status(400).json({
            message: 'Error: '+error,
        })
    });
}

function login(req, res) {
    let data = {
        email : req.fields.email,
        password : req.fields.password,
    }

    const { email, password } = data

    User.findOne({
        where: { email },
        // attributes: ['id', 'user_code', 'password', 'first_name', 'last_name', 'dob', 'email']
    }).then(user => {
        bcrypt.compareSync(password, user.password) ? 
            res.json({
                data: user,
                message: `Hi, Welcome back ${user.first_name} !`,
                token: getToken({
                    email: user.email, 
                })
            })
            : false
    })
}

export default {
    getAll,
    register,
    login
}