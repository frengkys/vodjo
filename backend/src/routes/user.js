import express from 'express';
import user from '../controller/user.ctrl';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(user.getAll)
  .post(user.register)

router.route('/login')
  .post(user.login);

export default router