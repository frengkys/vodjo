import express from 'express';
import user from './user';
import student from './student';
import journal from './journal';

const router = express.Router(); // eslint-disable-line new-cap

router.use('/user', user);
router.use('/student', student);
router.use('/journal', journal);

export default router;