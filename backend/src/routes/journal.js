import express from 'express';
import journal from '../controller/journal.ctrl';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(journal.getAll)
  .post(journal.store)

export default router